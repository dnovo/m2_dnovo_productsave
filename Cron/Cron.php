<?php


namespace Dnovo\Productsave\Cron;

use \Magento\Framework\App\ResourceConnection;
use \Psr\Log\LoggerInterface;
use Magento\Catalog\Api\ProductRepositoryInterface;
use \Magento\Indexer\Model\Indexer\CollectionFactory;

class Cron
{

    protected $logger;
    protected $resourceConnection;
    /**
     * @var ProductRepositoryInterface
     */
    protected $productRepository;

    protected $indexerCollectionFactory;

    /**
     * Cron constructor.
     * @param LoggerInterface $logger
     * @param ResourceConnection $resourceConnection
     * @param ProductRepositoryInterface $productRepository
     * @param CollectionFactory $indexerCollectionFactory
     */
    public function __construct(
        LoggerInterface $logger,
        ResourceConnection $resourceConnection,
        ProductRepositoryInterface $productRepository,
        CollectionFactory $indexerCollectionFactory
    )
    {
        $this->logger = $logger;
        $this->resourceConnection = $resourceConnection;
        $this->productRepository = $productRepository;
        $this->indexerCollectionFactory = $indexerCollectionFactory;
    }

    /**
     * Execute the cron
     *
     * @return void
     */
    public function execute()
    {
        $tableName = $this->resourceConnection->getTableName('catalog_product_entity');
        $today = date('Y-m-d 00:00:00');

        $sql = "SELECT `entity_id` FROM " . $tableName . " WHERE `created_at` > :created_at";
        $bind = array(
            ':created_at' => $today
        );
        $idList = $this->resourceConnection->getConnection()->fetchCol($sql, $bind);
        foreach($idList as $productId) {
            try {
                $product = $this->productRepository->getById($productId);
                $this->productRepository->save($product);

            }
             catch (\Exception $exception){
                $this->logger->error($exception->getMessage());
            }
        }
        try {
            $indexerCollection = $this->indexerCollectionFactory->create();
            foreach ($indexerCollection->getItems() as $indexer) {
                $indexer->reindexAll();
            }

        }catch (\Exception $exception) {
            $this->logger->error($exception->getMessage());

        }

    }
}